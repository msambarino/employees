FactoryGirl.define do
  factory :employee do
    first_name "First"
    last_name "Last"
    email { "#{first_name}.#{last_name}@vp.com".downcase }
    location "Montevideo, Uruguay"
    manager "Marcus"
  end

  factory :client do
    name "Name"
    email { "#{name}@vp.com".downcase }
    country "Uruguay"
    address "First Street 1234"
    phone "099123465"
    active "false"
  end

  factory :project do
    name "Project"
    client
  end

  factory :assignment do
    employee
    project
  end



end
