class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :assignments]

  # GET /employees
  def index
    query = request.query_parameters
    if query.empty? then
      @employees = Employee.all.includes(assignments: {project: :client})
    else
      validate_params
      @employees = Employee.where(query).includes(assignments: {project: :client})
    end
    render json: @employees
  end

  # GET /employees/1
  def show
    render json: @employee
  end

  def assignments
    @assignments = @employee.assignments

    render json: @assignments
  end

  # def as_json
  #   puts "as_json"
  #   super(:include => {
  #         :assignments => {
  #           :include =>
  #             {:project => {:only => [:project_id, :name]}}
  #         }
  #       }
  # )
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:email])
    end

    def validate_params
      ActionController::Parameters.new(request.query_parameters).permit(
        :location,
        :manager,
        :first_name,
        :middle_name,
        :last_name,
        :birthdate,
        :country,
        :city,
        location: [],
        manager: [],
        first_name: [],
        middle_name: [],
        last_name: [],
        birthdate: [],
        country: [],
        city: [])
    end

end
