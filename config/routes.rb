Rails.application.routes.draw do
  get 'clients/:id/projects/' => 'clients#show_projects'
  resources :projects, only: [:index, :show, :create, :update, :destroy]
  resources :clients,  only:  [:index, :show, :create, :update, :destroy] 

  resources :employees, param: :email, only: [:index, :show], :constraints => {:email => /[^\/]+/} do
    get "/assignments" => "employees#assignments"
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
