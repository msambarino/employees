class EmployeeSerializer < ActiveModel::Serializer
  attributes :email, :first_name, :middle_name, :last_name, :birthdate, :manager, :country, :city, :location
  has_many :assignments
end
