class Employee < ApplicationRecord
  self.primary_key = "email"
  has_many :assignments, foreign_key: :employee_email
  has_many :projects, :through => :assignments

end
