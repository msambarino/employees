require 'rails_helper'

RSpec.describe "Clients", type: :request do
  describe "GET /clients" do
    it "returns all clients" do
      FactoryGirl.create :client, name: "John", email: "john.doe@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: false
      FactoryGirl.create :client, name: "Jane", email: "jane.doe@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: false

      get "/clients"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)
      employees_names = body.map { |e| e["name"]}

      expect(employees_names).to match_array(["John","Jane"])
    end
  end

  describe "Filter clients" do
    it "returns clients by name" do
      john = FactoryGirl.create :client, name: "John", email: "john.doe@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: true

      get "/clients?name=John"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)
      body.map do |client|
          expect(client["name"]).to eq('John')
      end
    end

    it "returns clients by country" do
      john = FactoryGirl.create :client, name: "John", email: "john.doe@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: true
      pete = FactoryGirl.create :client, name: "Peter", email: "peter@vp.com", country: "Argentina", address: "Calle 1", phone: "099123456", active: true
      doc = FactoryGirl.create :client, name: "Doc", email: "doc@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: true
      steve = FactoryGirl.create :client, name: "Steve", email: "steve@vp.com", country: "Argentina", address: "Calle 1", phone: "099123456", active: true

      get "/clients?country=Uruguay"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)

      clients = body.map { |e| e["name"]}

      expect(clients).to match_array([john.name, doc.name])

    end

  end

  describe "GET /clients/:id/projects" do
    it "returns project name from client" do
      john = FactoryGirl.create :client, id:"11", name: "John", email: "john.doe@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: true
      vp1 = FactoryGirl.create :project, client: john, name: "VP1", description: "VP1 project", team_lead: "TL VP", solution_manager: "SM VP", active: true

      get "/clients/11/projects"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)
      body.map do |project|
          expect(project["name"]).to eq(vp1.name)
      end
    end
  end

  describe "GET /clients/:id/projects" do
    it "returns project name and client name from client" do
      john = FactoryGirl.create :client, id:"11", name: "John", email: "john.doe@vp.com", country: "Uruguay", address: "Calle 1", phone: "099123456", active: true
      vp1 = FactoryGirl.create :project, client: john, name: "VP1", description: "VP1 project", team_lead: "TL VP", solution_manager: "SM VP", active: true

      get "/clients/11/projects"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)
      body.map do |project|
          expect(project["name"]).to eq(vp1.name)
          expect(project["client"]["name"]).to eq(john.name)
      end
    end
  end

end
