class ProjectsController < ApplicationController
  before_action :set_project, only: [:show]

  # GET /projects
  def index
    @projects = Project.all

    render json: @projects
  end

 def create
    @project = Project.new({:name => params[:name], :description => params[:description], :client_id => params[:client_id], :team_lead => params[:team_lead], :solution_manager => params[:solution_manager], :active => params[:active]})
    if @project.save
      redirect_to @project
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render error
    end
  end

  # GET /projects/1
  def show
    render json: @project
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

end
