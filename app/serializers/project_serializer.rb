class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :client

  def client
        ActiveModelSerializers::SerializableResource.new(object.client, {})
  end
end
