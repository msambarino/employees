class Assignment < ApplicationRecord
  belongs_to :employee, foreign_key: "employee_email"
  belongs_to :project
end
