# Employees API

## Overview

Api documentation may be found at http://docs.vpadp.apiary.io/

## System Dependencies

* Ruby `>= 2.2`
* Rails `5.0.0`
* Postgresql `>= 9.2`

## Testing

Testing is done with `RSpec` and `FactoryGirl`, 
be sure to `bundle install` after cloning the project.

To test, run `rails spec`
