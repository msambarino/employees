class ClientsController < ApplicationController
  before_action :set_client, only: [:show]

  # GET /clients
  def index
    query = request.query_parameters
    if query.empty? then
      @clients = Client.all
    else
      validate_params
      @clients = Client.where(query)
    end

    render json: @clients
  end

 def create
    @client = Client.new({:name => params[:name], :email => params[:email], :country => params[:country], :address => params[:address], :phone => params[:phone], :active => params[:active]})
    if @client.save
      redirect_to @client
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render "new"
    end
  end

  # GET /clients/1
  def show
    render json: @client
  end

  #
  def show_projects
    @client = Client.find(params[:id])
    render :json => @client.projects
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    def validate_params
      ActionController::Parameters.new(request.query_parameters).permit(:name, :email, :country, :phone, [])
    end

    def clients_params
      params.require(:client).permit(:name, :email, :country, :address, :phone, :active)
    end
end
