class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  ActionController::Parameters.action_on_unpermitted_parameters = :raise

  rescue_from(ActionController::UnpermittedParameters) do |pme|
      render json: { error: { unknown_parameters: pme.params } },
             status: :bad_request
    end

  def not_found
    render json: {"error" => "not found"}, status: :not_found
  end
end
