class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees, id: false do |t|
      t.string :email, null: false
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.date :birthdate
      t.string :manager
      t.string :country
      t.string :city
      t.string :location

      t.timestamps
    end

    add_index :employees, :email, unique: true
  end
end
