require 'rails_helper'

RSpec.describe "Employees", type: :request do
  describe "GET /employees" do
    it "returns all employees" do
      FactoryGirl.create :employee, first_name: "John", last_name: "Doe"
      FactoryGirl.create :employee, first_name: "Jane", last_name: "Doe"

      get "/employees"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)
      employees_emails = body.map { |e| e["email"]}

      expect(employees_emails).to match_array(["jane.doe@vp.com","john.doe@vp.com"])
    end
  end

  it "returns employees with their assignments" do
    john = FactoryGirl.create :employee, first_name: "John", last_name: "Doe"
    jane = FactoryGirl.create :employee, first_name: "Jane", last_name: "Doe"
    vp = FactoryGirl.create :client, name: "VP"
    adp = FactoryGirl.create :project, name: "ADP", client: vp
    FactoryGirl.create :assignment, employee: john, project: adp
    FactoryGirl.create :assignment, employee: jane, project: adp

    get "/employees"
    expect(response).to have_http_status(200)

    body = JSON.parse(response.body)
    body.map do |employee|
      employee["assignments"].map do |assignment|
        expect(assignment["project"]["name"]).to eq(adp.name)
      end
    end
  end

  describe "Filter /employees" do
    it "returns employees from Montevideo" do
      FactoryGirl.create :employee, first_name: "John", location: "Montevideo"
      FactoryGirl.create :employee, first_name: "Jane",location: "Montevideo"
      FactoryGirl.create :employee, first_name: "Jennifer",location: "Buenos Aires"
      FactoryGirl.create :employee, first_name: "Carl",location: "Buenos Aires"

      get "/employees?location=Montevideo"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)

      employees_locations = body.map { |e| e["location"]}

      expect(employees_locations).to match_array(["Montevideo", "Montevideo"])

    end

    it "returns employees from Montevideo or Buenos Aires" do
      FactoryGirl.create :employee, first_name: "John", location: "Montevideo"
      FactoryGirl.create :employee, first_name: "Jane",location: "Montevideo"
      FactoryGirl.create :employee, first_name: "Jennifer",location: "Buenos Aires"
      FactoryGirl.create :employee, first_name: "Carl",location: "Barranquilla"

      get "/employees?location[]=Montevideo&location[]=Buenos%20Aires"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)

      employees_locations = body.map { |e| e["location"]}

      expect(employees_locations).to match_array(["Buenos Aires", "Montevideo", "Montevideo"])

    end

    it "returns employees from Montevideo managed by Sebastian" do
      FactoryGirl.create :employee, first_name: "John", location: "Montevideo", manager: "Sebastian"
      FactoryGirl.create :employee, first_name: "Jane",location: "Montevideo"
      FactoryGirl.create :employee, first_name: "Jennifer",location: "Buenos Aires", manager: "Sebastian"
      FactoryGirl.create :employee, first_name: "Carl",location: "Buenos Aires"

      get "/employees?location=Montevideo&manager=Sebastian"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)

      body.map do |e|
        expect(e["manager"]).to eq("Sebastian")
        expect(e["location"]).to eq("Montevideo")
      end
    end

    it "returns 400 (Bad Request) when an unknown parameter is given" do
      FactoryGirl.create :employee

      get "/employees?location=Montevideo&query=bad"
      expect(response).to have_http_status(400)
    end

  end

  describe "GET /employees/:email" do
    it "returns the requested employee's detail" do
      employee = FactoryGirl.create :employee, first_name: "Jane", last_name: "Doe"

      get "/employees/#{employee.email}"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)

      expect(body["email"]).to eq employee.email
      expect(body["first_name"]).to eq employee.first_name
      expect(body["last_name"]).to eq employee.last_name
    end

    it "returns not found when no employee with the given email exists" do
      get "/employees/nonexistent"
      expect(response).to have_http_status(404)
    end
  end

end
