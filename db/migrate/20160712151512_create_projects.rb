class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.belongs_to :client, foreign_key: true
      t.string :team_lead
      t.string :solution_manager
      t.boolean :active
      
      t.timestamps
    end
  end
end
