require "rails_helper"

RSpec.describe EmployeesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/employees").to route_to("employees#index")
    end

    it "routes to #show" do
      expect(:get => "/employees/john.doe@vp.com").to route_to("employees#show", :email => "john.doe@vp.com")
    end

  end
end
