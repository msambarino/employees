class CreateAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :assignments do |t|
      t.string :employee_email
      t.belongs_to :project, foreign_key: true
      t.date :start_date
      t.date :end_date
      t.string :notes

      t.timestamps
    end

    add_foreign_key :assignments, :employees, column: :employee_email, primary_key: "email"
  end
end
