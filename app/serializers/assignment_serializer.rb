class AssignmentSerializer < ActiveModel::Serializer
  attributes :project, :start_date, :end_date, :notes

  def project
    ActiveModelSerializers::SerializableResource.new(object.project, {})
  end
end
