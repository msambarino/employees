require 'rails_helper'

RSpec.describe "Projects", type: :request do
  describe "GET /projects" do
    it "returns all projectos" do
      vp1 = FactoryGirl.create :project, name: "Project1", description: "description of project one"
      vp2 = FactoryGirl.create :project, name: "Project2", description: "project 2 is better than one"

      get "/projects"
      expect(response).to have_http_status(200)

      body = JSON.parse(response.body)
      projects_names = body.map { |e| e["name"]}

      expect(projects_names).to match_array([vp1.name, vp2.name])
    end
  end
end
